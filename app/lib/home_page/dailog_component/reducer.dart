import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<DailogState> buildReducer() {
  return asReducer(
    <Object, Reducer<DailogState>>{
      DailogAction.init: _onInit,
    },
  );
}

DailogState _onInit(DailogState state, Action action) {
  final DailogState newState = state.clone();
  return newState;
}
