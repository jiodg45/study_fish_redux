import 'package:fish_redux/fish_redux.dart';

enum DailogAction { init, sure, cancel }

class DailogActionCreator {
  static Action onSure() {
    return const Action(DailogAction.sure);
  }

  static Action onCancel() {
    return const Action(DailogAction.cancel);
  }
}
