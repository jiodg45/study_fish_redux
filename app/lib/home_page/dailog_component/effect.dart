import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'action.dart';
import 'state.dart';

Effect<DailogState> buildEffect() {
  return combineEffects(<Object, Effect<DailogState>>{
    DailogAction.cancel: _onCancel,
    DailogAction.sure: _onSure,
  });
}

void _onCancel(Action action, Context<DailogState> ctx) {
  Navigator.of(ctx.context).pop('cancel');
}

void _onSure(Action action, Context<DailogState> ctx) {
  Navigator.of(ctx.context).pop('surce');
}
