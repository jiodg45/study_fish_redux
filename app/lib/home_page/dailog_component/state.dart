import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

class DailogState implements Cloneable<DailogState> {
  AlertDetails alertDetails;
  @override
  DailogState clone() {
    return DailogState();
  }
}

/// Get intial state args by navigator, only for page
DailogState _initState(Map<String, dynamic> args) {
  return DailogState()
    ..alertDetails = AlertDetails(
      title: args['title'],
      details: args['details'],
      surceTitle: args['sureTitle'],
      cancelTitle: args['cancelTitle'],
    );
}

class DailogConnector extends ConnOp<HomeState, DailogState> {
  @override
  DailogState get(HomeState state) {
    return _initState(state.args);
  }
}

class AlertDetails {
  final String title;
  final String details;
  final String surceTitle;
  final String cancelTitle;
  const AlertDetails(
      {this.title, this.details, this.surceTitle, this.cancelTitle,});
}
