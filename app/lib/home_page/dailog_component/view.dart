import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'state.dart';
import 'action.dart';

Widget buildView(DailogState state, Dispatch dispatch, ViewService viewService) {
  return AlertDialog(title: Text(state.alertDetails.title),content: Text(state.alertDetails.details),
    actions: [
      RaisedButton(child: Text(state.alertDetails.surceTitle), onPressed: (){
        dispatch(DailogActionCreator.onSure());
      },),
      RaisedButton(child: Text(state.alertDetails.cancelTitle), onPressed: (){
         dispatch(DailogActionCreator.onSure());
      },)
    ],
   );
}
