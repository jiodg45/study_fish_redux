import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class DailogComponent extends Component<DailogState> {
  DailogComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<DailogState>(
              adapter: null, slots: <String, Dependent<DailogState>>{}),
        );
}
