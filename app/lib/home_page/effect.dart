import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'action.dart';
import 'state.dart';

Effect<HomeState> buildEffect() {
  return combineEffects(<Object, Effect<HomeState>>{
    HomeAction.longPress: onLongPress,
    HomeAction.doubleTap: onDoubleTap,
  });
}

void onLongPress(Action action, Context<HomeState> ctx) {}

void onDoubleTap(Action action, Context<HomeState> ctx) {
    showDialog(builder: (context){
     return ctx.buildComponent('dailog');
   }, context: ctx.context,);
}
