import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(HomeState state, Dispatch dispatch, ViewService viewService) {
  // final listAdapter = viewService.buildAdapter();
  // return Scaffold(
  //   appBar: PreferredSize(
  //     preferredSize: Size.fromHeight(55.0),
  //     child: AppBar(
  //       automaticallyImplyLeading: false,
  //       centerTitle: true,
  //       elevation: 0.05,
  //       title: Text('home'),
  //     ),
  //   ),
  //   body: SmartRefresher(
  //     controller: state.refreshController,
  //     header: ClassicHeader(),
  //     child: ListView.builder(
  //       itemBuilder: listAdapter.itemBuilder,
  //       itemCount: listAdapter.itemCount,
  //     ),
  //     onRefresh: () {},
  //   ),
  // );
  return Scaffold(
    appBar: AppBar(
      title: Text('Home'),
    ),
    body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemBuilder: (context, index) {
          return Card(
            child: GestureDetector(
              child: Text('index: $index \n context: $context'),
              onTap: () {
                  dispatch(HomeActionCreator.onTap());
              },
              onDoubleTap: () {
                dispatch(HomeActionCreator.onDoubleTap());
              },
              onLongPress: () {
                 dispatch(HomeActionCreator.onLongPress());
              },
            ),
          );
        }),
  );
}
