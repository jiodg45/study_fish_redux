import 'package:fish_redux/fish_redux.dart';

class HomeState implements Cloneable<HomeState> {
  Map<String, dynamic> args;
  @override
  HomeState clone() {
    return HomeState();
  }
}

HomeState initState(Map<String, dynamic> args) {
  return HomeState()
    ..args = {
      'title': 'title',
      'details': 'details',
      'sureTitle': 'sureTitle',
      'cancelTitle': 'cancelTitle',
    };
}
