import 'package:fish_redux/fish_redux.dart';

enum HomeAction { tap, doubleTap, longPress }

class HomeActionCreator {
  static Action onTap() {
    return const Action(HomeAction.tap);
  }

    static Action onDoubleTap() {
    return const Action(HomeAction.doubleTap);
  }

    static Action onLongPress() {
    return const Action(HomeAction.longPress);
  }
}
