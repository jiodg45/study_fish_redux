import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';
import './dailog_component/component.dart';
import './dailog_component/state.dart';

class HomePage extends Page<HomeState, Map<String, dynamic>> {
  HomePage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<HomeState>(
              adapter: null,
              slots: <String, Dependent<HomeState>>{
                'dailog': DailogConnector() + DailogComponent(),
              }),
          middleware: <Middleware<HomeState>>[
            logMiddleware<HomeState>(tag: 'home'),
          ],
        );
}
