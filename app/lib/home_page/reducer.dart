import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<HomeState> buildReducer() {
  return asReducer(
    <Object, Reducer<HomeState>>{
      HomeAction.tap: _onTap,
    },
  );
}

HomeState _onTap(HomeState state, Action action) {
  final newState = state.clone();
  return newState;
}
