import 'package:fish_redux/fish_redux.dart';
import 'package:app/home_page/page.dart';
import 'package:flutter/material.dart' hide Page;
import '../root_page/page.dart';
import '../login_page/page.dart';
import '../register_page/page.dart';

class AppRoutes {
  static const String root = '/';
  static const String register = '/register';
  static const String login = '/login';
  static const String home = '/home';

  static Route<Object> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) =>
          routes.buildPage(settings.name, settings.arguments) ??
          Container(
            child: Center(
              child: Text('unknow route $settings'),
            ),
          ),
    );
  }

  static final AbstractRoutes routes =
      PageRoutes(pages: <String, Page<Object, dynamic>>{
    root: RootPage(),
    home: HomePage(),
    register: RegisterPage(),
    login: LoginPage(),
  });
}
