import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import '../routes/routes.dart';
import 'action.dart';
import 'state.dart';

Effect<RootState> buildEffect() {
  return combineEffects(<Object, Effect<RootState>>{
    RootAction.login: _onLogin,
    RootAction.register: _onRegister,
  });
}

void _onLogin(Action action, Context<RootState> ctx) {
  Navigator.of(ctx.context).pushNamed(AppRoutes.login);
}

void _onRegister(Action action, Context<RootState> ctx) {
  Navigator.of(ctx.context).pushNamed(AppRoutes.register);
}
