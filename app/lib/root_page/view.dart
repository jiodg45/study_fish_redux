import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'action.dart';

import 'state.dart';

Widget buildView(RootState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    appBar: PreferredSize(
      preferredSize: Size.fromHeight(55.0),
      child: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0.05,
        title: Text('RootPage'),
      ),
    ),
    body: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(height: 100),
          Center(
            child: FlutterLogo(size: 40.0),
          ),
          SizedBox(height: 20),
          Text('state.list: ${state.list}'),
          SizedBox(height: 20),
          RaisedButton(
            onPressed: () {
              dispatch(RootActionCreator.onLogin());
            },
            child: Text('Login'),
          ),
          SizedBox(height: 20),
          RaisedButton(
            onPressed: () {
              dispatch(RootActionCreator.onRegister());
            },
            child: Text('Register'),
          ),
          SizedBox(height: 20),
          RaisedButton(
            onPressed: () {
              dispatch(RootActionCreator.onRefresh());
            },
            child: Text('Refresh'),
          )
        ],
      ),
    ),
  );
}
