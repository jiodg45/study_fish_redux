import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RootState> buildReducer() {
  return asReducer(
    <Object, Reducer<RootState>>{
      RootAction.refresh: _onRefresh,
    },
  );
}

RootState _onRefresh(RootState state, Action action) {
  final RootState newState = state.clone();
  newState.list = (state.list..add(state.list.last + 1));
  return newState;
}
