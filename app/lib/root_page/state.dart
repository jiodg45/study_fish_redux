import 'package:fish_redux/fish_redux.dart';

class RootState implements Cloneable<RootState> {
  List<int> list;
  @override
  RootState clone() {
    return RootState()..list = (list..add(list.last + 1));
  }
}

RootState initState(Map<String, dynamic> args) {
  return RootState()..list = [1, 2, 3];
}
