import 'package:fish_redux/fish_redux.dart';

enum RootAction { login, register, refresh }

class RootActionCreator {
  static Action onLogin() {
    return const Action(RootAction.login);
  }

  static Action onRegister() {
    return const Action(RootAction.register);
  }

  static Action onRefresh() {
    return const Action(RootAction.refresh);
  }
}
