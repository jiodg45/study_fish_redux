import 'package:flutter/material.dart' hide Page, Action;
import 'package:app/routes/routes.dart';
import 'utils.dart/storage.dart';

void main() {
  runApp(
    Shell(),
  );
}

class Shell extends StatefulWidget {
  @override
  _ShellState createState() => _ShellState();
}

class _ShellState extends State<Shell> {
  Future<void> _coreSeviceInitilized;
  @override
  void initState() {
    super.initState();
    _coreSeviceInitilized = StarupService.initilized();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _coreSeviceInitilized,
      builder: (context, snapShot) {
        switch (snapShot.connectionState) {
          case ConnectionState.done:
            return MaterialApp(
              theme: ThemeData.light(),
              initialRoute: AppRoutes.root,
              onGenerateRoute: AppRoutes.onGenerateRoute,
            );
          default:
            return CircularProgressIndicator();
        }
      },
    );
  }
}
