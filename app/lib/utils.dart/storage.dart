import 'package:shared_preferences/shared_preferences.dart';

class StarupService {
  static Future<void> initilized() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }
}

/// 1. init [SharedPreferences]
SharedPreferences get sharedPreferences => _sharedPreferences;
SharedPreferences _sharedPreferences;
