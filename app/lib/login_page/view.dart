import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(LoginState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    appBar: AppBar(
      title: Text('Login'),
    ),
    body: GestureDetector(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 80.0),
                FlutterLogo(size: 120.0),
                SizedBox(height: 20.0),
                TextField(
                  controller: state.userNameEditingController,
                  textInputAction: TextInputAction.next,
                  textAlign: TextAlign.start,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    icon: Icon(Icons.phone),
                    labelText: 'phone',
                    hintText: 'enter your phone number',
                    errorText: state.userNameErrorText,
                    errorStyle: TextStyle(
                      fontSize: 14.0,
                      color: Colors.red,
                    ),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        state.pwdFocusNode.requestFocus();
                      },
                    ),
                  ),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]{0,11}')),
                  ],
                  onEditingComplete: () {
                    dispatch(LoginActionCreator.create(LoginAction.checkLogin));
                    state.pwdFocusNode.requestFocus();
                  },
                  onSubmitted: (text) {
                    dispatch(LoginActionCreator.create(LoginAction.checkLogin));
                  },
                  maxLength: 11,
                  focusNode: state.userNameFocusMode,
                ),
                SizedBox(height: 20.0),
                TextField(
                  controller: state.userPwdEditingController,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    icon: Icon(Icons.security),
                    labelText: 'password',
                    hintText: 'enter your password',
                    errorText: state.userPwdErrorText,
                    errorStyle: TextStyle(fontSize: 14.0, color: Colors.red),
                  ),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                        RegExp(r'[0-9a-zA-Z]{1,11}')),
                  ],
                  obscureText: false,
                  focusNode: state.pwdFocusNode,
                  onEditingComplete: () {
                    dispatch(LoginActionCreator.create(LoginAction.checkLogin));
                  },
                  onSubmitted: (text) {
                    dispatch(LoginActionCreator.create(LoginAction.checkLogin));
                  },
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  onPressed: state.isLoginEnable
                      ? () {
                          dispatch(
                              LoginActionCreator.create(LoginAction.submitted));
                        }
                      : null,
                  color: Colors.blue,
                  disabledColor: Colors.blue.withAlpha(127),
                  child: Text('Login'),
                )
              ],
            ),
          ),
        ),
        onTap: () {
          FocusScope.of(viewService.context).requestFocus(new FocusNode());
          dispatch(LoginActionCreator.create(LoginAction.checkLogin));
        }),
  );
}
