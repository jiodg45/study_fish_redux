import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import '../routes/routes.dart';
import '../utils.dart/storage.dart';
import 'action.dart';

import 'state.dart';

Effect<LoginState> buildEffect() {
  return combineEffects(<Object, Effect<LoginState>>{
    LoginAction.submitted: _onSubmitted,
  });
}

void _onSubmitted(Action action, Context<LoginState> ctx) {
  sharedPreferences.setString(
      'userName', ctx.state.userNameEditingController.text);
  sharedPreferences.setString(
      'userPwd', ctx.state.userPwdEditingController.text);
  Navigator.of(ctx.context).popAndPushNamed(AppRoutes.home);
}
