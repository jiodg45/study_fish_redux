import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import '../utils.dart/storage.dart';
import '../utils.dart/storage.dart';
import '../utils.dart/storage.dart';

class LoginState implements Cloneable<LoginState> {
  TextEditingController userNameEditingController;
  TextEditingController userPwdEditingController;

  String userNameErrorText;
  String userPwdErrorText;
  FocusNode userNameFocusMode;
  FocusNode pwdFocusNode;

  bool isLoginEnable;

  @override
  LoginState clone() {
    return LoginState()
      ..userNameEditingController = userNameEditingController
      ..userPwdEditingController = userPwdEditingController
      ..userNameErrorText = userNameErrorText
      ..userPwdErrorText = userPwdErrorText
      ..isLoginEnable = isLoginEnable
      ..userNameFocusMode = userNameFocusMode
      ..pwdFocusNode = pwdFocusNode;
  }
}

LoginState initState(Map<String, dynamic> args) {
  final userName = sharedPreferences.getString('userName');
  final userPwd = sharedPreferences.getString('userPwd');
  return LoginState()
    ..userNameEditingController = TextEditingController(text: userName)
    ..userPwdEditingController = TextEditingController(text: userPwd)
    ..userNameFocusMode = FocusNode()
    ..pwdFocusNode = FocusNode()
    ..userNameErrorText = null
    ..userPwdErrorText = null
    ..isLoginEnable = _getUserNameErrorString(userName) == null &&
        _getUserPasswordErrorString(userPwd) == null;
}

String _getUserNameErrorString(String userName) {
  if (userName == null || userName.isEmpty) return 'userName could not be null';
  if (RegExp(r'[1-9]{1,}').allMatches(userName).length == 0)
    return 'userName should be 0-9 digits';
  if (userName.length <= 6) return 'useName lenght should be more then size';
  return null;
}

String _getUserPasswordErrorString(String password) {
  if (password == null || password.isEmpty) return 'userName could not be null';
  if (RegExp(r'[1-9a-zA-Z]{1,}').allMatches(password).length == 0)
    return '0 to 6 digits and upper and lower case letters';
  if (password.length <= 6) return 'useName lenght should be more then size';
  return null;
}
