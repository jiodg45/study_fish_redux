import 'package:fish_redux/fish_redux.dart';

enum LoginAction {
  clearUserName,
  clearPassword,
  checkUserName,
  checkPassword,
  checkLogin,
  submitted,
}

class LoginActionCreator {
  static Action create(LoginAction action) {
    return Action(action);
  }
}
